# Git cheatsheet

A concise summary of Git commands and everything related to it.

Markdown cheatsheet is available at [github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).

## Git basics

**Basic commands:**
```
git clone git@gitlab.com:kostas4/git-cheatsheet.git <folder-name>
git pull
git fetch --all
git push
```

**Branches:**
```
git checkout <existing-branch-name>
git checkout -b <new-branch-name>
```

**Push branch to remote and track:**
```
git checkout -b <feature-branch-name>
git push -u origin <feature-branch-name>
```

**Delete local and remote branch:**
```
git push -d origin <existing-branch-name>
git branch -d <existing-branch-name>
```

**Combined add, commit and push:**
```
git add -A && git commit -m "commit message" && git push
```

**Setting your branch to exactly match the remote branch:**
```
git fetch origin
git reset --hard origin/<branch-name>
```

### Git file operations

**Revert a single file:**
```
git reset <filename>
```

**Remove file from git, but keep local copy (e.g. after adding it to .gitignore):**
```
git rm --cached
git commit -m "removed files from git"
git push
```

### Force-delete Git history

```
git checkout --orphan newBranch
git add -A  # Add all files and commit them
git commit
git branch -D master  # Deletes the master branch
git branch -m master  # Rename the current branch to master
git push -f origin master  # Force push master branch to github
git gc --aggressive --prune=all     # remove the old files
```

Taken from [here](https://stackoverflow.com/questions/9683279/make-the-current-commit-the-only-initial-commit-in-a-git-repository).

## Vagrant

**Basic VM controls:**
```
vagrant up
vagrant destroy
vagrant reload --provision
vagrant global-status
vagrant ssh
```
**Plugins:**
```
vagrant plugin install vagrant-hostsupdater
vagrant plugin uninstall vagrant-hostsupdater
```

### Homestead

**Updating Homestead:**

```
vagrant destroy
git fetch
git pull origin release
composer update
vagrant box update
vagrant up
```
